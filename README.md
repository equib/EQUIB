## EQUIB
[![Build Status](https://travis-ci.org/equib/EQUIB.svg?branch=master)](https://travis-ci.org/equib/EQUIB)
[![Binder](http://mybinder.org/badge.svg)](http://mybinder.org/repo/equib/equib)
[![GitHub license](https://img.shields.io/aur/license/yaourt.svg)](https://github.com/equib/EQUIB/blob/master/LICENSE)

**Program EQUIB**  (FORTRAN 90)

The Fortran program EQUIB solves the statistical equilibrium equation for each ion and yields atomic level populations and line emissivities for given physical conditions, namely electron temperature and electron density, appropriate to the zones in an ionized nebula where the ions are expected to exist. 

### Installation

How to compile:

    make

How to clean:

    make clean

### References

* I.D. Howarth, S. Adams, et al., [Astrophysics Source Code Library, ascl:1603.005, 2016](http://adsabs.harvard.edu/abs/2016ascl.soft03005H)

* I.D. Howarth & S. Adams, [University College London, 1981](http://adsabs.harvard.edu/abs/1981ucl..rept.....H)

* I.D. Howarth & B. Wilson, [MNRAS, 204, 1091, 1983](http://adsabs.harvard.edu/abs/1983MNRAS.204.1091H)

* S. Adams & M.J. Seaton, [MNRAS, 200, 7P, 1982](http://adsabs.harvard.edu/abs/1982MNRAS.200P...7A)

* J.P. Harrington, M.J. Seaton, S. Adams & J.H. Lutz, [MNRAS, 199, 517, 1982](http://adsabs.harvard.edu/abs/1982MNRAS.199..517H)

* J.P. Harrington, [ApJ, 152, 943, 1968](http://adsabs.harvard.edu/abs/1968ApJ...152..943H)

* J.P. Harrington, [Ph.D.Thesis, Ohio State University, 1967](http://adsabs.harvard.edu/abs/1967PhDT.........6H)